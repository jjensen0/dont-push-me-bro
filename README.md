Don't Push Me Bro
======================
General
----
The Don't Push Me Bro project expands the capabilities of the CEENBoT with the new ADXL345 chip. The chip allows the CEENBoT to measure forces of acceleration on three different axes. With this chip, the CEENBoT could be programmed to measure its tilt or direction. 

The goal of the Don't Push Me Bro project was to use the ADXL345 to create a program which allowed the CEENBoT to track its own movement and return to the origin point. The ability to track and identify movement from a fixed location is known as dead reckoning. This is a method of motion tracking that is used in everything from cars to video games. The approach was to take the acceleration data and integrate it twice to have positional data and thus the distance from the origin point.

The two files uploaded to the repository consist of two ways to communicate with the ADXL345 external sensor in order to accomplish the dead reckoning.  These two methods of communication are I2C and SPI.  Both of these files utilize the CEENBoT API.  For more information on the CEENBoT API and how to use it, please visit the following web page:

http://www.ceen.unomaha.edu/CEENBoT/


License
----
Copyright (C) 2014 The Board of Regents of the University of Nebraska.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Credits
--------
Don't Push Me Bro was created by Trevor Hoke.