/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Auth: Trevor Hoke

//	This program Dont_Push_Me_Bro
//	was made to demonstrate the ADXL345, and possibliy the GY-521 6DOF MPU6050 
//	by allowing the robot to keep track of angles in which it was pushed and return to the original positions.

// Program was altered from previous I2C_Basic
// still altering
// backup before demo

 /****************************************************
 *	pin connector, guide from chip to CEENbot Board	 *
 *													 *
 *	chip |  20pin |pin lable						 *
 *	-----+--------+-----------						 *
 *	GND .....20.......GND							 *
 *	VCC .....18.......3.3V							 *
 *	CS  .....18.......3.3V							 *
 *	INT1.....NA.......NA							 *
 * 	INT2.....NA.......NA							 *
 *	SDO .....20.......GND							 *
 *	SDA .....13.......MEGA 324 SDA					 *
 *	SCL .....12.......MEGA 324 SCL					 *
 *****************************************************/
 
#include "capi324v221.h"
#include <string.h>


// =========================== global variables =================================== //
/* With ALT ADDRESS (SDO) tied to ground, device address is 0x53 */
#define ADXL345_ADDR 				0x53	// I2C data righting ADDR
#define ADXL345_BW_RATE				0x2C	// Data rate recieving 
#define ADXL345_POWER_CTRL          0x2D    // Power-saving features control
#define ADXL345_DATA_FORMAT         0x31    // Data format control
#define ADXL345_DATAX0              0x32    // X-axis data 0
#define ADXL345_DATAX1              0x33    // X-axis data 1
#define ADXL345_DATAY0              0x34    // Y-axis data 0
#define ADXL345_DATAY1              0x35    // Y-axis data 1
#define ADXL345_DATAZ0              0x36    // Z-axis data 0
#define ADXL345_DATAZ1              0x37    // Z-axis data 1

/* Buffer to store incoming I2C data */
char i2c_data[10];

/* Variables to store x, y, and z axis values */
int x;
int y;
int z;

float ScaleForFullG = 0.0039;// for full range

// From calibration
int	yCalibration      =0;
int calibration_count =0;
int Ybias             =0;

// From Sample
int y_sample     = 0;
int sample_count = 1;
float y_accel    = 0;


// integration information
float y_velocity      = 0;
float prev_y_velocity = 0;
float prev_y_accel    = 0;

//integration 2 info
float y_position      = 0;
float prev_y_position = 0;

float time = 0.045632; // 400Hz is 400^-sec is 2500 micro seconds, at 32 readings per 400Hz gives 80,000microsec... or 0.08 second updates?

// data limit loop
int count   = 0;

// oops reset for velocity
int noAccel = 0;

int Hey_Arnold = 0;
int movement = 0;

// ================================ prototypes ====================================== //

SPI_CFGR(ADXL345);
void ADXL345_write( char registerAddress, char value );
void ADXL345_read( unsigned char registerAddress, int numBytes, char *i2c_data );

// ================================ Functions ======================================= //
// custom SPI configuration for ADXL345
SPI_CFGR( ADXL345 )
{
	// Setup the SPCR register (see data-sheet of ATmega324 for details).
   	SPCR = (1<<SPE)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(0<<SPR0)|(0<<SPR1); // set up modes, in order, SPI, Master, CPOL, CPHA, SPR0 and SPR1 divides frequency by 128
	
	SPSR = 0; // clears flag

   SPSR = ( 0 << SPI2X );     // Set up clock divider (Fosc/64).
   	
	DELAY_us( 10 );            	// Wait a bit.
}

void Setup()
{
	STEPPER_open();
	SPI_open();											// Configure the SPI
    SPI_set_config_func( SPI_DEV_ADDR3, ADXL345 );		// Register custom SPI config routine & associate it with SS 3
	
	LCD_open();
	LCD_clear();
	ATTINY_open();


	ADXL345_write( ADXL345_POWER_CTRL,  0x00 );
	ADXL345_write( ADXL345_DATA_FORMAT, 0x0B );// & set D0 to put ADXL345 into +/- 16G range
 	ADXL345_write( ADXL345_BW_RATE,     0x0F );// Should set The system to low power, and 400Hz
	ADXL345_write( ADXL345_POWER_CTRL,  0x08 );// Put ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTRL reg. 

	UART_open( UART_UART0 );
	/* Configure the UART:
     *
     * Data Size = 8-bits
     * Stop Bits = 1
     * Parity    = None
     * Baud      = 57600bps 	
	 */
	UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 57600);
 	UART_set_TX_state( UART_UART0, UART_ENABLE );//Enable the transmitter.
}

/*
void ADXL345_write( char registerAddress, char value )
{
	I2C_STATUS status;
	
	// Initiate communication transaction with ADXL345 at I2C address 
	status = I2C_MSTR_start( ADXL345_ADDR, I2C_MODE_MT );
	
	// If there is a problem, show it on LCD screen.
	if (status != I2C_STAT_OK)
		LCD_printf( "Error with start write [ 0x%X ].\n", status );
	
	// Send register address
	status = I2C_MSTR_send( registerAddress );
	if (status != I2C_STAT_OK)
		LCD_printf( "Error with write addr [ 0x%X ].\n", status );
	
	// Send value to register address
	status = I2C_MSTR_send( value );
	if (status != I2C_STAT_OK)
		LCD_printf( "Error with write val [ 0x%X ].\n", status );
	
	// Complete transaction.
	I2C_MSTR_stop();
}*/
void ADXL345_write( char registerAddress, char value )
{
	SPI_set_slave_addr( SPI_ADDR_NA ); // sets to 7 

  	DELAY_ms( 1 );

	SPI_set_slave_addr( SPI_DEV_ADDR3 ); // sets to 3

    SPI_transmit( SPI_DEV_ADDR3, registerAddress ); // transmits address

	SPSR = 0; // resets flag

    SPI_transmit( SPI_DEV_ADDR3, value ); // transmits value

  	DELAY_ms( 1 );

   SPI_set_slave_addr( SPI_ADDR_NA ); // sets to seven
} 

/*void ADXL345_read( unsigned char registerAddress, int numBytes, char *i2c_data )
{
	I2C_STATUS status;
	
	// Set MSB to read from address
    unsigned char address = 0x80 | registerAddress;

	// Set A6 to read multiple bytes
    if ( numBytes > 1 )
        address = address | 0x40;

	// Master Transmit --> transmit address where we will start reading from
	status = I2C_MSTR_start( ADXL345_ADDR, I2C_MODE_MT );
	if (status != I2C_STAT_OK)
		LCD_printf( "Error with start read [ 0x%X ].\n", status );
	
	// Send address from which we'll start multi-byte read
	status = I2C_MSTR_send( address );
	if (status != I2C_STAT_OK)
		LCD_printf( "Error with read addr [ 0x%X ].\n", status );

	// 
	if ( I2C_MSTR_start( ADXL345_ADDR, I2C_MODE_MR ) == I2C_STAT_OK )
	{                                                                                      
		I2C_MSTR_get_multiple( i2c_data, numBytes );
	}
	else
		LCD_printf( "Error with read [ 0x%X ].\n", status );
	
	// end transaction
	I2C_MSTR_stop();
}*/

void ADXL345_read( unsigned char registerAddress, int numBytes, char *spi_data )
{
	unsigned int i;

	// Set MSB to read from address
    char address = 0x80 | registerAddress;

	// Set A6 to read multiple bytes
    if ( numBytes > 1 )
        address = address | 0x40;

	SPI_set_slave_addr( SPI_ADDR_NA );

	DELAY_us( 500 );

	SPI_set_slave_addr( SPI_DEV_ADDR3 ); 
	
	// Transfer the starting register address that needs to be read
	SPI_transmit( SPI_DEV_ADDR3, address );
    
	for ( i = 0; i < numBytes; i++ )
	{
        spi_data[ i ] = SPI_receive(SPI_DEV_ADDR3, SPI_NULL_DATA); // 
    }
    
	SPI_set_slave_addr( SPI_ADDR_NA );        // Done -- select NULL device
}


void Calibrate()
{
	while(calibration_count < 1024) // why 1000 because, it is larger then 500, and gives results
	{// take accel value, 
		
		DELAY_us(313);
		ADXL345_read( ADXL345_DATAX0, 4, i2c_data );
			

	        x = ((int)i2c_data[1]<< 8 | (int)i2c_data[0]);
			y = ((int)i2c_data[3]<< 8 | (int)i2c_data[2]);
			

			UART0_printf("0%d\t %d\n", calibration_count, y  );

			yCalibration += y;

			calibration_count++;
	}

		Ybias = yCalibration/1024;

}

void Sample()
{
	while(sample_count <=64 )// why 64 because, that is why
		{// take accel value, 

		//	UART0_printf("s_c: %d\n",sample_count  );
			DELAY_us(313);
		//	DELAY_us(2500);
			
			ADXL345_read( ADXL345_DATAX0, 4, i2c_data );

		        x = ((int)i2c_data[1]<< 8 | (int)i2c_data[0]);
				y = ((int)i2c_data[3]<< 8 | (int)i2c_data[2]);

				y_sample += y;

				sample_count++;
		}	
	
	y_accel = (int)(y_sample/sample_count);
	
	sample_count = 1;
	y_sample = 0;

}

/* check it :
 * Start with integration as a rectangle ontop of square
 * Sample(n-1)*time + (Sample(n)-Sample(n-1))*0.5*time + prev_integration
 * thus
 * (Sample(n-1)+(Sample(n)-Sample(n-1))/2)*time +prev_integration
 */

void Integration()
{
	// First y-integration
	y_velocity = prev_y_velocity + (prev_y_accel + ((y_accel - prev_y_accel)/2.0))*time;
	
	// Second y-integration
	y_position = prev_y_position + (prev_y_velocity +((y_velocity - prev_y_velocity)/2.0))*time;
}

/*while(run == 3)
			{

			STEPPER_move( STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
						  STEPPER_REV, 200, 200, 400, STEPPER_BRK_OFF, NULL, // left
					   	  STEPPER_REV, 200, 200, 400, STEPPER_BRK_OFF, NULL ); // step, speed, accel. 
			
				run++;
			}
			
						STEPPER_SPEED curr_speed;
						curr_speed = STEPPER_get_curr_speed();*/
// ================================ CBOT main ======================================= //
void CBOT_main()
{	
	Setup();
	
	while(!ATTINY_get_SW_state(ATTINY_SW4))
	{
		LCD_printf("START: S4 To Calibrat\t\n\n");
	}
	DELAY_ms(500);	
	Calibrate();
	
	UART0_printf("END cal: %d\n", Ybias   );

	while(!ATTINY_get_SW_state(ATTINY_SW4))
	{
		LCD_printf("START: S4 To sample\t\n\n\n");
				
	}
	DELAY_ms(500);
	UART0_printf("count\ty_accel\ty_velocity\ty_position\n");

	while( 1 )
    {
		LCD_printf("Go!\t");
		
		while(count < 1000)
		{

			Sample();
		
			y_accel-= Ybias;

		// discrimination window :why place in seperate thing... 
			if(-3 <= y_accel && y_accel <= 3)
			{
				y_accel = 0;
		
			}
			else
				{
				//	y_accel = y_accel;
					y_accel *= ScaleForFullG; // Multiplied by the gain to be in G's
					y_accel *= 9.8;// m/s^2 yeah... have to do this,
				}
		// end window
		
			Integration();	


			//UART0_printf("0%d\t%f\t\n", count, y_accel);


			UART0_printf("0%d\t%f\t%f\t%f\n", count, y_accel,y_velocity,y_position);
			
	
			if(y_accel == 0)
			{
				noAccel++;
			}else
				{
					noAccel = 0;
				}
			
			if( noAccel >= 6)
			{
				prev_y_velocity = 0;
				y_velocity      = 0;
				noAccel         = 0;
			}
	
	
	
	
	
		// reset of values for next integration
			prev_y_accel    = y_accel;
			prev_y_velocity = y_velocity;
			prev_y_position = y_position;
		// end of that process
		
			count++;


			if(y_accel == 0)
			{
				Hey_Arnold++;
			}else
				{
					Hey_Arnold = 0;
				}
		
			while( Hey_Arnold == 400)
			{
					
					
					movement *= 749;
				
					UART0_printf("steps: %d\n",movement);


					STEPPER_move( STEPPER_STEP_NO_BLOCK, STEPPER_BOTH,
						  STEPPER_FWD, movement, 200, 400, STEPPER_BRK_OFF, NULL, // left
					   	  STEPPER_FWD, movement, 200, 400, STEPPER_BRK_OFF, NULL ); // step, speed, accel. 


					Hey_Arnold == 0;

			}
			



		}
	} 

} // end CBOT_main()
